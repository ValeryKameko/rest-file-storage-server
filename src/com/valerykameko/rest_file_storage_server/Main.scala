package com.valerykameko.rest_file_storage_server

import java.net.{BindException, InetSocketAddress}
import java.nio.file.{InvalidPathException, Path}
import java.util.concurrent.Executors

import com.sun.net.httpserver.HttpServer

object Main {
  private val DEFAULT_SERVER_PORT = 8080

  def main(args: Array[String]): Unit = {
    parseArgs(args)

    try {
      val server = HttpServer.create(new InetSocketAddress(Configs.port), 0)
      server.createContext("/", new RootHandler())
      server.setExecutor(Executors.newCachedThreadPool())
      server.start()
    }
    catch {
      case error : BindException =>
        println("Cannot start server: " + error.getMessage)
        System.exit(1)
    }
  }

  def parseArgs(args: Array[String]): Unit = {
    try {
      args.length match {
        case 1 =>
          Configs.dataDirectory = Path.of(args(0))
        case 2 =>
          Configs.dataDirectory = Path.of(args(0))
          Configs.port = Integer.parseUnsignedInt(args(1))
        case _ =>
      }
    }
    catch {
      case _ : NumberFormatException | _ : InvalidPathException =>
        println("Wrong format of args")
        System.exit(1)
      case _: Throwable =>
        System.exit(1)
    }
  }
}
