package com.valerykameko.rest_file_storage_server.services

import java.nio.file.Files

import com.valerykameko.rest_file_storage_server.Configs

class MoveService {
  def moveFile(fromSubPath: String, toSubPath: String): Unit = {
    val fromFile = Configs.dataDirectory.resolve(fromSubPath)
    val toFile = Configs.dataDirectory.resolve(toSubPath)
    Files.move(fromFile, toFile)
  }
}
