package com.valerykameko.rest_file_storage_server.services

import java.io.{File, FileInputStream, FileNotFoundException, InputStream, OutputStream}
import java.nio.file.{Files, LinkOption, OpenOption, Path, Paths, StandardOpenOption}

import com.sun.nio.file.ExtendedOpenOption
import com.sun.nio.file.ExtendedOpenOption._
import com.valerykameko.rest_file_storage_server.Configs

class GetService {
  def getFile(subPath: String): InputStream = {
    val file = Configs.dataDirectory.resolve(subPath)
    if (!Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS))
      throw new FileNotFoundException(file.toString)
    Files.newInputStream(file, StandardOpenOption.READ)
  }
}
