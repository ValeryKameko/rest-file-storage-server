package com.valerykameko.rest_file_storage_server.services

import java.io.{FileOutputStream, InputStream, OutputStream}

import com.valerykameko.rest_file_storage_server.Configs

class PostService {
  def appendFile(subPath: String, inStream: InputStream): Unit = {
    val file = Configs.dataDirectory.resolve(subPath)
    val saveStream: OutputStream = new FileOutputStream(file.toFile, true)
    try {
      Iterator
        .continually(inStream.read)
        .takeWhile(byte => byte != -1)
        .foreach(saveStream.write)
    } finally {
      if (saveStream != null)
        saveStream.close()
    }
  }

}
