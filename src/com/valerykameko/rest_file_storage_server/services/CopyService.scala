package com.valerykameko.rest_file_storage_server.services

import java.nio.file.{CopyOption, Files, StandardCopyOption}

import com.valerykameko.rest_file_storage_server.Configs

class CopyService {
  def copyFile(fromSubPath: String, toSubPath: String): Unit = {
    val fromPath = Configs.dataDirectory.resolve(fromSubPath)
    val toPath = Configs.dataDirectory.resolve(toSubPath)
    Files.copy(fromPath, toPath)
  }
}
