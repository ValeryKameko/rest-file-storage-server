package com.valerykameko.rest_file_storage_server.services

import java.io.FileNotFoundException
import java.nio.file.Files

import com.valerykameko.rest_file_storage_server.Configs

class DeleteService {
  def deleteFile(subPath: String): Unit = {
    val file = Configs.dataDirectory.resolve(subPath)
    if (!Files.isRegularFile(file))
      throw new FileNotFoundException(file.toString)
    Files.delete(file)
  }
}
