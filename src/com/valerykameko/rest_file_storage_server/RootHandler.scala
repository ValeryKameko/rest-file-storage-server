package com.valerykameko.rest_file_storage_server

import java.net.HttpURLConnection._

import com.sun.net.httpserver.{HttpExchange, HttpHandler}
import com.valerykameko.rest_file_storage_server.controllers.{CopyController, DeleteController, GetController, MoveController, PostController, PutController}

class RootHandler extends HttpHandler {
  override def handle(httpExchange: HttpExchange): Unit = {
    httpExchange.getRequestMethod match {
      case "GET" =>
        (new GetController).handle(httpExchange)
      case "PUT" =>
        (new PutController).handle(httpExchange)
      case "POST" =>
        (new PostController).handle(httpExchange)
      case "DELETE" =>
        (new DeleteController).handle(httpExchange)
      case "COPY" =>
        (new CopyController).handle(httpExchange)
      case "MOVE" =>
        (new MoveController).handle(httpExchange)
    }
  }
}

object RootHandler {
  def responseNotFound(httpExchange: HttpExchange): Unit = {
    httpExchange.sendResponseHeaders(HTTP_NOT_FOUND, 0)
  }

  def responseInternalError(httpExchange: HttpExchange): Unit = {
    httpExchange.sendResponseHeaders(HTTP_INTERNAL_ERROR, 0)
  }

  def responseOk(httpExchange: HttpExchange): Unit = {
    httpExchange.sendResponseHeaders(HTTP_OK, 0)
  }

  def responseBadRequest(httpExchange: HttpExchange): Unit = {
    httpExchange.sendResponseHeaders(HTTP_BAD_REQUEST, 0)
  }
}
