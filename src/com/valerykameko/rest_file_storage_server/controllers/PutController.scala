package com.valerykameko.rest_file_storage_server.controllers

import com.sun.net.httpserver.{HttpExchange, HttpHandler}
import com.valerykameko.rest_file_storage_server.RootHandler
import com.valerykameko.rest_file_storage_server.services.PutService

class PutController extends HttpHandler {
  val service = new PutService

  override def handle(httpExchange: HttpExchange): Unit = {
    try {
      service.saveFile(httpExchange.getRequestURI.getPath.substring(1), httpExchange.getRequestBody)

      RootHandler.responseOk(httpExchange)
      httpExchange.close()
    } catch {
      case error : Throwable =>
        RootHandler.responseInternalError(httpExchange)
    } finally {
      httpExchange.close()
    }
  }
}
