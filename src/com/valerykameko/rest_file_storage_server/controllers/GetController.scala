package com.valerykameko.rest_file_storage_server.controllers

import java.io.{FileNotFoundException, IOException, InputStream, OutputStream}
import java.nio.file.{NoSuchFileException, Path}

import com.sun.net.httpserver.{HttpExchange, HttpHandler}
import com.valerykameko.rest_file_storage_server.RootHandler
import com.valerykameko.rest_file_storage_server.services.GetService

class GetController extends HttpHandler {
  val service = new GetService

  override def handle(httpExchange: HttpExchange): Unit = {
    var inStream : Option[InputStream] = None
    var outStream : Option[OutputStream] = None

    try {
      inStream = Some(service.getFile(httpExchange.getRequestURI.getPath.substring(1)))
      outStream = Some(httpExchange.getResponseBody)

      RootHandler.responseOk(httpExchange)
      Iterator
        .continually(inStream.get.read)
        .takeWhile(byte => byte != -1)
        .foreach(outStream.get.write)
    } catch {
      case _ : NoSuchFileException | _ : FileNotFoundException =>
        RootHandler.responseNotFound(httpExchange)
      case error : Throwable =>
        RootHandler.responseInternalError(httpExchange)
    } finally {
      inStream.foreach(_.close)
      outStream.foreach(_.close)
      httpExchange.close()
    }
  }
}