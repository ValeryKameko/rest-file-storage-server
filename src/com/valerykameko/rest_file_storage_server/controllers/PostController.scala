package com.valerykameko.rest_file_storage_server.controllers

import com.sun.net.httpserver.{HttpExchange, HttpHandler}
import com.valerykameko.rest_file_storage_server.RootHandler
import com.valerykameko.rest_file_storage_server.services.PostService

class PostController extends HttpHandler {
  val service = new PostService

  override def handle(httpExchange: HttpExchange): Unit = {
    try {
      service.appendFile(httpExchange.getRequestURI.getPath.substring(1), httpExchange.getRequestBody)

      RootHandler.responseOk(httpExchange)
      httpExchange.close()
    } catch {
      case error : Throwable =>
        RootHandler.responseInternalError(httpExchange)
    } finally {
      httpExchange.close()
    }
  }
}
