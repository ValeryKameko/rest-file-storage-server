package com.valerykameko.rest_file_storage_server.controllers

import java.io.FileNotFoundException
import java.nio.file.NoSuchFileException

import com.sun.net.httpserver.{HttpExchange, HttpHandler}
import com.valerykameko.rest_file_storage_server.RootHandler
import com.valerykameko.rest_file_storage_server.services.MoveService

class MoveController extends HttpHandler {
  val service = new MoveService

  override def handle(httpExchange: HttpExchange): Unit = {
    try {
      val source = httpExchange.getRequestURI.getPath
      val queryRegex = """destination=(.*)""".r
      httpExchange.getRequestURI.getQuery match {
        case queryRegex(destination) => {
          service.moveFile(source.substring(1), destination.substring(1))
          RootHandler.responseOk(httpExchange)
        }
        case _ => RootHandler.responseBadRequest(httpExchange)
      }
    } catch {
      case _ : FileNotFoundException | _ : NoSuchFileException =>
        RootHandler.responseNotFound(httpExchange)
      case error : Throwable =>
        RootHandler.responseInternalError(httpExchange)
    } finally {
      httpExchange.close()
    }
  }

}
