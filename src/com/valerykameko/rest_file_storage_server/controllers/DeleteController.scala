package com.valerykameko.rest_file_storage_server.controllers

import java.io.FileNotFoundException
import java.nio.file.NoSuchFileException

import com.sun.net.httpserver.{HttpExchange, HttpHandler}
import com.valerykameko.rest_file_storage_server.RootHandler
import com.valerykameko.rest_file_storage_server.services.DeleteService

class DeleteController extends HttpHandler {
  val service = new DeleteService

  override def handle(httpExchange: HttpExchange): Unit = {
    try {
      service.deleteFile(httpExchange.getRequestURI.getPath.substring(1))

      RootHandler.responseOk(httpExchange)
    } catch {
      case _ : FileNotFoundException | _ : NoSuchFileException =>
        RootHandler.responseNotFound(httpExchange)
      case error : Throwable =>
        RootHandler.responseInternalError(httpExchange)
    } finally {
      httpExchange.close()
    }
  }
}
