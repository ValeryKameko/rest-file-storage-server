package com.valerykameko.rest_file_storage_server

import java.nio.file.Path

object Configs {
  var port: Int = 8080
  var dataDirectory: Path = Path.of("./data")
}
